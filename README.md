# XMLoaf 🍞

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Handle your XML data in a relaxed, laid-back way!

`XMLoaf` is a rudimentary FatScript XML codec implementation.

> XML attributes and self-closing tags are not supported.

## Installation

To use `XMLoaf`, ensure you have `fry` (^2.5.1), the [FatScript](https://fatscript.org) interpreter, installed.

**Use the [chef](https://gitlab.com/fatscript/chef) package manager**:

```bash
chef menuadd fatscript.org
chef include xmloaf
chef restock
```

OR

**Clone the this repository**:

```bash
git clone https://gitlab.com/aprates/xmloaf.git
```

## How to Use

### Import

Ensure `xmloaf.fat` is within your FatScript project folder and import it as follows:

```fat
_ <- xmloaf
```

If using [chef](https://gitlab.com/fatscript/chef), the module would likely be located in the `stock` folder:

```fat
_ <- stock.xmloaf
```

### Settings

Adjust this variable to configure the behavior of the processing functions:

- `xmlWarnings`, default is `true` - set to `false` to suppress XML warnings.

### XML Functions

| Name    | Signature         | Brief                        |
| ------- | ----------------- | ---------------------------- |
| toXML   | (node: Any): Text | Encode XML from native types |
| fromXML | (text: Text): Any | Decode XML into native types |

### Example

Building XML from native types:

```
data = {
  bookstore = [
    {
      book = {
        title = 'Book 1'
        author = 'Author 1'
      }
    }
  ]
}

xmlString = toXML(data)
# xmlString will be the xml representation of the data
```

> `toXML` generates xml string from FatScript data structures

Parsing XML back into native types:

```
xmlData =
  '<bookstore><book><title>Book 1</title><author>Author 1</author></book></bookstore>'

parsedData = fromXML(xmlData)
# parsedData will be a Scope containing the parsed xml data
```

> lists are automatically inferred when multiple sibling items are present, which might lead to inconsistent data structures in cases where an element is expected to be a list but occasionally contains only a single item, or even none

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [XMLoaf GitLab](https://gitlab.com/aprates/xmloaf/issues) page.

### Donations

If you find `XMLoaf` useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## License

[MIT License](LICENSE) © 2024 Antonio Prates.
